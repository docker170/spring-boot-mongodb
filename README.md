# spring-boot-mongodb
This repository contains a Spring Boot example project for MongoDB.

# How to run this application
```
1. Using Docker Container (Non Persitence Storage)
  s1
  - run mongodb docker container(mdc).
  s2
  - docker run -p 27018:27017 mongo:latest
    -- 27018 - mongo port that this app will use to connect to mdc
    -- 27017 - mdc exposed port
  s3
  - check state of mdc
    -- docker ps
  s4
  - bootstrap sprint boot app (mvn spring-boot:run)
  
2. Using Docker Container (Using Persitence Storage)
  s1
  - run mongodb docker container(mdc).
  s2
  - docker run -p 27018:27017 -v /my/own/dir:/data/db mongo:latest
    -- 27018 - mongo port that this app will use to connect to mdc
    -- 27017 - mdc exposed port
    -- /my/own/dir - is host machine dir, replace this as per the host
  s3
  - check state of mdc
    -- docker ps
  s4
  - bootstrap sprint boot app (mvn spring-boot:run)
  
3. Without Using Docker Container
  s1
  - run mongo server on port 27018
  s2
  - bootstrap sprint boot app (mvn spring-boot:run)
```
